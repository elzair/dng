% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/dng.R
\name{aw}
\alias{aw}
\title{Add Wall Wrapper}
\usage{
aw(y, x, side, value = "w", auto.draw = TRUE)
}
\arguments{
\item{y}{The vertical coordinate of the wall unit}

\item{x}{The horizontal coordinate of the wall unit}

\item{side}{The wall side: "t" for top, "l" for left, "r" for right,
and "b" for bottom}

\item{value}{The type of wall unit to add: "w": Wall, "d": Door,
"iw": Illusionary Wall, "hd": Hidden Door (default: "w")}

\item{auto.draw}{Automatically display map after addition (default: TRUE)}
}
\description{
This function is a convenience wrapper for \code{\link{add.wall}}.
}
\examples{
# Make wrap-around map look like graph paper
make.map("Test Map", wrap.around = TRUE)
aw(0:15, 0:15, "t")
aw(0:15, 0: 15, "l")
# Make normal map look like graph paper
make.map("Test Map")
aw(0:15, 0:15, "b")
aw(15, 0:15, "t")
aw(0:15, 0:15, "l")
aw(0:15, 15, "r")
}
\seealso{
\code{\link{add.wall}} which this function wraps
}

