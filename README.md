# dng

**dng** is a mapping utility for old-school grid-based computer (and possibly pen & paper) role-playing games. It uses the [R programming environment](https://www.r-project.org/) and its excellent plotting facilities to provide an interactive way to create area maps as you explore an area. Unlike many other mapping solutions (including pen & grid paper), it does not require fine motor skills since all input is entered from a command line. You also do not need to know much about either programming in general or **R** in particular to use **dng** (although it helps).

The best way to show you the awesomeness of *dng* is to plot an area. For this example, we will use the beginning area of [Pool of Radiance](https://en.wikipedia.org/wiki/Pool_of_Radiance): the "Civilized" District of New Phlan.

![](https://gitlab.com/elzair/dng/raw/master/pool-of-radiance-screenshot.png)

Assume we have already installed both **R** and **dng** and have started an **R** session. We type the following commands into the **R** console. Press **Enter** after you have typed the full line to run your command.

## Example: Pool of Radiance

First, we load **dng**.

```R
library(dng)
```

Next, we create our initial map, which we name "Civilized District". Since *Pool of Radiance* coordinates start at the top-left corner instead of the bottom-left corner (as most games do), we tell **dng** to invert the y-coordinate. We also tell it to flip the x & y coordinates since *Pool of Radiance* specifies them as x & y instead of y & x.

```R
make.map("Civilized District", invert.y = TRUE, flip.coords = TRUE)
```

This produces the following display.

![](https://gitlab.com/elzair/dng/raw/master/ex-01.png)

Now let's add the door going to the slums. The following command adds a wall to the left side of cell (x=0,y=4).

```R
add.wall(0, 4, "l")
```

**NOTE:** `add.wall()` recognizes any of the following characters as cardinal directions:

* **u** -- Up
* **d** -- Down
* **l** -- Left
* **r** -- Right
* **n** -- North
* **s** -- South
* **w** -- West
* **e** -- East
* **t** -- Top 
* **b** -- Bottom

![](https://gitlab.com/elzair/dng/raw/master/ex-02.png)

Oops! We wanted a door not a plan old wall! However, we can easily undo our change.

```R
undo()
```

**NOTE:** We can give `undo()` a numerical argument, such as `undo(5)`, to undo the five most recent changes.

![](https://gitlab.com/elzair/dng/raw/master/ex-01.png)

If we want any type of wall besides the default, we have to specify a fourth argument. 

```R
add.wall(0, 4, "l", "d")
```

![](https://gitlab.com/elzair/dng/raw/master/ex-03.png)

If we turn to the north, we will see walls to our left and right going from y=3 to y=1. We can add both walls at once; the following code will add left walls to cells (0, 1), (1, 1), (0, 2), (1, 2), (0, 3) & (1,3).


```R
add.wall(0:1, 1:3, "l")
```

**NOTE:** We can press the **Up** and **Down** arrow keys to cycle through our previous input. Instead of typing out the entirety of the last input, we can press **Up** to get the previous input and add `, "d"` before `)`, change `4` to `1:3` and add `:1` to `0`. We do not have to specify the value for a wall, `"w"`, since that is the default.

![](https://gitlab.com/elzair/dng/raw/master/ex-04.png)

Now, let's scout around the perimeter of New Phlan.

**NOTE:** We can save time by typing `aw()` instead of `add.wall()`. 

```R
aw(0, 0, "l")
aw(0:4, 0, "t")
aw(1, 0, "b", "d")
aw(2:3, 0, "b")
aw(4, 0, "r")
aw(4, 1, "l", "d")
aw(5, 1, "t")
aw(5, 1:2, "r", "d")
aw(6:9, 3, "t")
aw(8:10, 3, "b")
aw(10, 3, "r")
aw(10, 1:2, "l")
aw(10, 1, "r")
aw(10, 0, "l", "d")
aw(10:12, 0, "t")
aw(11:12, 0, "b")
aw(12, 0, "r")
aw(11, 2:3, "t", "d")
aw(12, 2, "t")
aw(12, 2, "r")
aw(13:15, 3:4, "t")
aw(15, 3, "r")
aw(12, 3:4, "l")
aw(12, 4, "r")
aw(13:15, 5:6, "t")
aw(15, 5, "r")
aw(12, 6, "r")
aw(13:15, 7, "t")
aw(15, 7, "r")
aw(15, 7:8, "b")
aw(14, 8, "r", "d")
aw(15, 9, "r")
aw(15, 9, "b", "d")
aw(14, 9, "b")
aw(14, 8, "l")
aw(13, 7, "b")
aw(13, 9, "t", "d")
aw(13:14, 10, "l")
aw(13, 11, "b")
aw(14:15, 11, "t")
aw(15, 11, "r")
aw(15, 11, "b")
aw(14:15, 12:14, "l")
aw(14, 14, "r", "d")
aw(14, 14, "b")
aw(12, 11, "t")
aw(12, 11:12, "l")
aw(12, 12:14, "r")
aw(3:12, 14, "b")
aw(3, 14, "l", "d")
aw(3:4, 12:13, "l")
aw(2, 11, "b", "d")
aw(0, 11, "b", "d")
aw(1:2, 12, "l")
aw(1, 12, "b")
aw(0, 10:11, "l")
aw(0, 10, "t", "d")
aw(0, 10, "r")
aw(1, 11, "t", "d")
aw(2, 10, "l")
aw(2, 10, "t", "d")
aw(3, 5:10, "r")
aw(3, 4, "r", "d")
aw(3, 5, "l", "d")
aw(3, 7, "l", "d")
aw(3, 9, "l", "d")
aw(2, 6:7, "t")
aw(2, 8:9, "t")
aw(2, 6, "l")
aw(2, 8, "l")
aw(0:2, 4, "b")
aw(1, 4:5, "t", "d")
aw(4, 14, "t")
aw(6, 14, "t")
aw(8:10, 14, "t")
aw(9, 14, "t", "d")
```

Whew! That was a lot of work! We should have a map that looks like this.

![](https://gitlab.com/elzair/dng/raw/master/ex-05.png)

Before we continue, let's save our work.

```R
save.work()
```

This saves our work to the file *Civilized District.map* in our current directory. If we wish to quit **R**, we type the following.

```R
quit()
```

Press the *n* key at the prompt. When we want to resume your map, we change to the directory where *Civilized District.map* is, start an R session, and type the following.

```R
library(dng) # We have to type this every time we start a new R session.
load.work("Civilized District")
```

Now, let's map the exterior of every building.

```R
aw(2, 2:3, "l")
aw(2:3, 2, "t")
aw(4, 3, "b", "d")
aw(5, 4, "l", "d")
aw(5:6, 4, "b")
aw(7, 4, "r", "d")
aw(7, 5:10, "l")
aw(4:6, 11, "t")
aw(4, 11, "b", "d")
aw(6, 11, "b", "d")
aw(5:6, 12:13, "l")
aw(7, 11, "r", "d")
aw(7, 12, "l", "d")
aw(7, 13, "l")
aw(7, 13, "r")
aw(8, 12, "t")
aw(8, 12, "b", "d")
aw(9, 12, "t", "d")
aw(9, 12, "b")
aw(10, 12, "b", "d")
aw(10, 11:12, "r", "d")
aw(10, 9:11, "l")
aw(10, 10, "l", "d")
# NOTE: We can modify existing walls (and cells). Here we replaced the left wall at 10,10 with a door
aw(10, 10, "r")
aw(10:11, 9, "t")
aw(11:12, 9, "b", "d")
aw(12:13, 8, "l")
aw(11, 13, "t")
aw(11, 13, "l")
aw(8:11, 7, "b")
aw(10, 7, "b", "d")
aw(8:10, 7, "t")
aw(7, 8:11, "r")
aw(7, 9, "r", "d")
aw(7, 11, "r", "d")
aw(7, 6, "r", "d")
aw(8:9, 5:6, "t")
aw(9, 5, "r", "d")
aw(11, 6, "l")
aw(11, 5, "l", "d")
aw(11, 5, "t")
```

![](https://gitlab.com/elzair/dng/raw/master/ex-06.png)

Now, let's finally go inside some of the buildings and map them out.

```R
aw(0, 9, "l")
aw(0, 9, "t")
aw(1, 8, "l")
aw(0, 7:8, "t")
aw(0, 7, "l")
aw(1, 6, "l")
aw(0, 5, "b")
aw(0, 5, "l")
aw(13, 1, "l", "d")
aw(13:15, 1:2, "t")
aw(15, 1, "r")
aw(6, 0:2, "r", "d")
aw(6, 0, "l", "d")
aw(5:9, 0, "t")
aw(7, 1, "t")
aw(8:9, 1, "t", "d")
aw(8:9, 0, "l")
aw(10, 5:6, "t")
aw(8, 8:9, "r")
aw(8:9, 9:10, "b")
aw(8, 11, "r")
aw(9, 10, "l", "d")
aw(0:2, 14, "t")
aw(0, 12:14, "l")
aw(0:2, 14, "b")
aw(15, 8:10, "r")
aw(15, 12:14, "r")
aw(15, 14, "b")
aw(9, 13, "r")
aw(11, 10:11, "b")
aw(11, 10, "r")
aw(4, 5, "r", "d")
aw(4, 6:10, "r")
aw(4, 6, "b", "d")
aw(5, 5, "b", "d")
aw(5, 5, "r", "d")
aw(6, 5, "b")
aw(5, 6, "b")
aw(6, 6, "b", "d")
aw(4, 7, "r", "d")
aw(4, 9, "r", "d")
aw(5, 8, "b")
aw(6, 8, "b", "d")
```

![](https://gitlab.com/elzair/dng/raw/master/ex-07.png)

This looks pretty good, but let's fill in the cells that have water.

```R
add.cell(0:15, 15, "water")
add.cell(13, 12:14, "water")
add.cell(13:15, 0, "water")
add.cell(13:15, 2, "water")
add.cell(13:15, 4, "water")
add.cell(13:15, 6, "water")
```

![](https://gitlab.com/elzair/dng/raw/master/ex-08.png)

We can also add annotations in the various cells. The following code replicates the annotations listed in the *Pool of Radiance* cluebook.

**NOTE:** `add.cell()` also has a shorter wrapper:  `ac()`.

```R
ac(15, 1, "1")
ac(11, 1, "2")
ac(10, 4, "3")
ac(9, 6, "3")
ac(10, 5, "4")
ac(8, 2, "5")
ac(5, 0, "6")
ac(7, 0, "7")
ac(8, 0, "8")
ac(9, 0, "9")
ac(1, 1, "10")
ac(1, 7, "11")
ac(1, 7, "11")
ac(4, 3, "12")
ac(5, 5, "13")
ac(6, 5, "14")
ac(6, 6, "15")
ac(5, 7, "16")
ac(5, 9, "17")
ac(1, 13, "18")
ac(1, 14, "19")
ac(4, 12, "19")
ac(6, 12, "19")
ac(8, 9, "20")
ac(10, 8, "20")
ac(15, 10, "20")
ac(15, 13, "20")
ac(8, 11, "21")
ac(8, 13, "21")
ac(11, 12, "21")
ac(13, 8, "21")
ac(9, 10:11, "22")
ac(11, 11, "22")
ac(12, 10, "22")
ac(15, 8, "22")
ac(11, 10, "23")
ac(10, 13, "23")
ac(8, 10, "24")
```

We finally get this.

![](https://gitlab.com/elzair/dng/raw/master/ex-09.png)

Lastly, let's export our map to a PNG file.

```R
save.png()
```

This creates the image file *Civilized District.png* in our current directory that we can view in any image viewer.

## Install

Currently, **dng** needs to be installed manually. With hope that will soon change.

### Manual

To install **dng** manually, you will first need a few things.

* The [R Programming Environment](https://www.r-project.org/)
* [git](https://git-scm.com/)

1. First, start an *R* session.
2. Next, install the *R* package "devtools": `install.packages("devtools")`
3. You will likely be asked to select a CRAN mirror. Choose one near your location.
4. Load the "devtools" library: `library(devtools)`
5. Install **dng**: `devtools::install_git("https://gitlab.com/elzair/dng.git")`

This should install **dng** onto your system.

## Commands

### make.map
This function creates a new map object with the given parameters
and makes it the current map. It also resets the undo history.

+ **title** The name of the map
+ **height** The height, in cells, of the map (default: 16) 
+ **width** The width, in cells, of the map (default: 16) 
+ **zero.indexed** Whether coordinates should start from 1 or 0 (default: TRUE)
+ **wrap.around** Whether the furthest column of vertical walls and furthest row of horizontal walls should mirror the first column and row respectively (default: FALSE)
+ **invert.y** If true, y coordinates go top-down; otherwise, bottom-up (default: FALSE)
+ **flip.coords** If true, calls to add.wall() and add.cell() will invert y and x values (default: FALSE)
+ **auto.draw** Automatically display map after creation (default: TRUE)

### resize.map

This function resizes the current map.

+ **height** The new height
+ **width** The new width
+ **x.offset** The x coordinate for the old origin (0,0)
+ **y.offset** The y coordinate for the old origin (0,0)
+ **auto.draw** Automatically display map after creation (default: TRUE)

### add.wall & aw

This function adds a wall unit with the given parameters to the current map. It also adds the old map to the undo history.

+ **y** The vertical coordinate of the wall unit
+ **x** The horizontal coordinate of the wall unit
+ **side** The wall side: "t" for top, "l" for left, "r" for right, and "b" for bottom
+ **value** The type of wall unit to add: "w": Wall, "d": Door, "iw": Illusionary Wall, "hd": Hidden Door (default: "w")
+ **auto.draw** Automatically display map after addition (default: TRUE)

### add.cell & ac

This function adds a cell unit with the given parameters to the current map. It also adds the old map to the undo history.

+ **y** The vertical coordinate of the cell unit
+ **x** The horizontal coordinate of the cell unit
+ **value** The type of cell unit to add; if any of the following strings are specified, the map will display a special symbol; otherwise, the value is displayed verbatim: "darkness", "elevator", "encounter", "downstairs", "pit", "spinner", "trap", "upstairs", and "water"
+ **auto.draw** Automatically display map after addition (default: TRUE)

### save.png

This function saves the current map to a png file specified by the map's title.

### save.work

This function saves your current map to disk for later use.

### load.work

This function loads a map from disk and makes it the current map. It also resets the undo history.

+ **title** The name of the map to load
+ **auto.draw** Automatically display map after load (default: TRUE)

### undo

This function will retrieve from the undo history a previous version of the current map. It will also purge from the undo history all instances created after the specified point.

+ **n** The number of changes to go back by
+ **auto.draw** Automatically display map after undo (default: TRUE)
